This update set creates a table, "Workflow Step" [u_workflow_step], modifies the default view of "Catalog Item" [sc_cat_item], and creates a workflow.

To use this, set the workflow of a item to "Standard Workflow".  Then create steps in the "Workflow Steps" section of the form.  A few things to note;

1.  This only supports single threaded tasks and approvals.  So you can make a task or approval and continue doing that, but only one will fire at a time.
2.  If you need the variables to show up on the catalog task, I'd suggest you just make the variables "global: true" and control visibilit with ui policies
3.  If you need the variables on other task types, the workflow will probably need to be extended to create records on [question_answer] appropriately.
4.  Once the workflow has been started, it queues all the steps in the workflow, so if the steps change later, it will continue as it was when the item [sc_req_item] was generated.
5.  I changed the allowable activies on the "Standard workflow" from 100 to 1000.  It's about 9-10 activies per loop, so at 100, ~ 10 could run, at 1000, 100 could run.